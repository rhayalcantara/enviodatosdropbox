﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Dropbox.Api;
using Dropbox.Api.Files;
using System.Data.SqlClient;
using Excel = Microsoft.Office.Interop.Excel;
using System.Reflection;
using System.Data;
using System.Configuration;

namespace EnviosDatosDropbox
{
    class Program
    {
        
        static  void Main(string[] args)
        {
              Program.tarea();
            //Console.Read();
        }
        static void tarea()
        {
            try
            {

                //"192.168.7.5"; //
                //To get the location the assembly normally resides on disk or the install directory
                string path = Environment.GetCommandLineArgs()[0];
                Console.WriteLine(path);
                String servidor = ConfigurationManager.AppSettings["Servidor"].ToString();
                //once you have the path you get the directory with:
                var directory = System.IO.Path.GetTempPath();
                String filepath_grabar_excel = "e:\\comisiones\\";
                String filename_excel_origen = "DW.xlsx";
                String filepath_grabar_dropbox = "/CoopAspire DB/incentivos/";
                
                Console.Write("Iniciando {0}", Environment.NewLine);
                String sql = "Declare @tmp table(sucursal varchar(3),idnofne varchar(3),codiofne int,"
                                          + "codisucu int,descofne varchar(100),carteraactiva float,tasapromedio float,"
                                          + "ingresos float,fondeo float,costofondeo float,gastosprovision float,"
                                          + "carteraaportaciones float,carterainverciones float,carteraahorros float,"
                                          + "CarteraPasiva float,TasaPasiva float,atrazo float,"
                                          + "mora float,estaofic varchar(1),mfm float,tasabase float,morodidadbase float,generado float,"
                                          + "smb float,sbmf float,inctapagar float);"
                                          + "insert @tmp EXEC comisioncreditos @FechaCorte;"
                                          + "select * from @tmp; ";
                String filename = "IncentivosSobreMargenFinancieroNeto";
                realizarcalculo(filepath_grabar_excel,
                                        filename_excel_origen,
                                        sql,
                                        filename,
                                        servidor,
                                        filepath_grabar_dropbox,
                                        DateTime.Now);
                sql = "Declare @tabla table(CODISUCU int,CODIOFNE int,DESCOFNE varchar(100),"
                                          + "CNT_SOCIOS int,CNT_ATRAZO1 int,netos int,CNT_SOCIOS_AHORR int,cnt_sin_actividad int,"
                                          + "cnt_netos int,cnt_socios_aportaciones int,cnt_sin_mov_aportacion int,"
                                          + "cnt_neto_aportacion int,cnt_socios_inverciones int,cnt_cruzados int,"
                                          + "total_socios int,porcientocruzado decimal(10, 8),incentivoagrupado decimal(12, 2),"
                                          + "incentivoneto decimal(12, 2),totalsociosanterior int,porcientocrecimiento decimal(10, 8),"
                                          + "apagar decimal(12, 2));"
                                          + " insert @tabla "
                                          + " exec [dbo].[comisionahorros] @FechaCorte;"
                                          + " select* from @tabla; ";
                filename = "IncentivosSobreCantidadNetaClientes";
                realizarcalculo(filepath_grabar_excel,
                                        filename_excel_origen,
                                        sql,
                                        filename,
                                        servidor,
                                        filepath_grabar_dropbox,
                                        DateTime.Now);
               //Console.Read();
            }
            catch (Exception ex)
            {
                Console.Write("{0}{1}", ex.Message, Environment.NewLine);
            }
        }
      static  void  realizarcalculo(String filepath_grabar_excel,
                                          String filename_excel_origen,
                                          String sql,
                                          String filename,
                                          String servidor,
                                          String filepath_grabar_dropbox,
                                          DateTime fecha)
        {
            try
            {
                
                SqlConnectionStringBuilder c = new SqlConnectionStringBuilder( String.Format("Data Source={0};Initial Catalog=Financ;Integrated Security=True",servidor));

                using (SqlConnection konekcija = new SqlConnection(c.ConnectionString))
                {

                    konekcija.Open();
                    Console.Write("Consultado Datos de {0} del servidor {1} {2}", filename,servidor, Environment.NewLine);
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Connection = konekcija;

                        cmd.CommandType = CommandType.Text;
                        cmd.CommandText = sql;
                        cmd.CommandTimeout = 0;
                       
                        // DateTime oDate = DateTime.Parse(fecha);
                        cmd.Parameters.AddWithValue("@FechaCorte", fecha);

                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            // Fill the DataSet using default values for DataTable names, etc
                            DataSet dataset = new DataSet();
                            da.Fill(dataset);


                            String fechas = fecha.ToString("ddMMyyyy"); //DateTime.Now.ToString("ddMMyyyyHHmmss");
                            String horas = DateTime.Now.ToString("HHmmss");
                            filename = String.Format("{0}{1}", filename, fechas + horas);

                            using (StreamWriter mylogs = new StreamWriter(filepath_grabar_excel + filename+".csv"))         //se crea el archivo
                                {

                                //se adiciona alguna información y la fecha
                                String s = "";
                                for (int i = 0; i < dataset.Tables[0].Columns.Count; i++)
                                {
                                    s += dataset.Tables[0].Columns[i].ColumnName+',';

                                }
                                s = s.Substring(0, s.Length - 1); //eliminar la ultima coma
                                mylogs.WriteLine(s);
                                s = "";
                                for (int i = 0; i < dataset.Tables[0].Rows.Count; i++)
                                {
                                    for (int ii = 0; ii < dataset.Tables[0].Columns.Count; ii++)
                                    {
                                        DataRow row = dataset.Tables[0].Rows[i];
                                        s+=row[dataset.Tables[0].Columns[ii].ColumnName].ToString()+',' ;

                                    }
                                    s = s.Substring(0, s.Length - 1); // eliminar la ultima coma
                                    mylogs.WriteLine(s);
                                    s = "";
                                }

                               
                                    mylogs.Close();

                                //await Class1.sendfile(filepath_grabar_excel,
                                //             filename + ".txt",
                                //             filepath_grabar_dropbox);
                                Console.Write("Terminado {0}", Environment.NewLine);
                            }
                            

                            
                        }
                    }
                }
            }
            catch (Exception ee)
            {
               Console.Write(ee.Message);

            }

        }


    }
}

